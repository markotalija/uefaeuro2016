//
//  AccountsViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/8/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "AccountsViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface AccountsViewController()
@property (weak, nonatomic) IBOutlet UIButton *myLoginButton;
@end

@implementation AccountsViewController

#pragma mark - Actions

- (IBAction)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loginButtonClicked {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions:@[@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else {
            NSLog(@"Logged in");
        }
    }];
}

- (IBAction)googleLogin {
    [[GIDSignIn sharedInstance] signIn];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    //Facebbok button click
    [self.myLoginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    //Google
    [GIDSignIn sharedInstance].uiDelegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end