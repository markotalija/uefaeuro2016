//
//  PushViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/6/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "PushViewController.h"

@interface PushViewController()
@end

@implementation PushViewController

#pragma mark - Actions

- (IBAction)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchButtonTapped:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    //Allow push notifications to occur on physical device, status bar.
    if (sender.selected) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
}

- (IBAction)doneButtonTapped {
    [self performSegueWithIdentifier:@"HomeSegue" sender:self];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIStatusBarStyle)preferredStatusBarStyle  {
    return UIStatusBarStyleLightContent;
}

@end