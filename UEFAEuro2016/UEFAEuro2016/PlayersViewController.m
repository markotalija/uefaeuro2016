//
//  PlayersViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "PlayersViewController.h"
#import "PlayersTableViewCell.h"
#import "DataManager.h"
#import "Player.h"

@interface PlayersViewController() <UITableViewDataSource, UITableViewDelegate>
@end

@implementation PlayersViewController

#pragma mark - Actions

- (IBAction)backButtonTapped {
    [self. navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchButtonTapped {
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.dataManager = [DataManager sharedInstance];
    [self.dataManager fillData];
    self.dictionary = [self.dataManager prepareDictionary];
    
    self.itemsArray = [self.dictionary.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [self.itemsArray objectAtIndex:section];
    NSArray *value = [self.dictionary valueForKey:key];
    
    return value.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlayersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSString *key = [self.itemsArray objectAtIndex:indexPath.section];
    NSArray *value = [self.dictionary valueForKey:key];
    
    Player *player = [value objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", player.fName, player.lName];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [self.itemsArray objectAtIndex:section];
    
    return key;
}

@end