//
//  DataManager.h
//  UEFAEuro2016
//
//  Created by Apple on 7/18/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface DataManager : NSObject
@property (strong, nonatomic) NSArray *sideMenuArray;
+ (instancetype)sharedInstance;
- (void)fillData;
- (NSMutableDictionary *)prepareDictionary;
+ (void)saveCustomObjectToUserDefaults:(id)object forKey:(NSString *)key;
+ (id)loadCustomObjectFromUserDefaultsForKey:(NSString *)key;
@end