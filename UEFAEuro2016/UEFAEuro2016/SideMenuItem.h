//
//  SideMenuItem.h
//  UEFAEuro2016
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SideMenuItem : NSObject
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) UIImageView *image;

- (instancetype)initWithText:(NSString *)text andImage:(UIImage *)image;

@end