//
//  ContainerViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ContainerViewController.h"
#import "SideMenuViewController.h"
#import "Constants.h"
#import "BaseViewController.h"
#import "Helpers.h"

@interface ContainerViewController()
@property (strong, nonatomic) UINavigationController *homenavigationController;
@property (strong, nonatomic) SideMenuViewController *sideMenu;
@end

@implementation ContainerViewController

#pragma mark - Public API

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserverForName:OPEN_MENU_NOTIFICATION object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if (self.sideMenu.isOpened) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CLOSED_MENU_NOTIFICATION object:nil];
            return;
        }
        
        [UIView animateWithDuration:kAnimationDuration animations:^{
            CGRect frame = self.homenavigationController.view.frame;
            frame.origin.x = kMenuOffset;
            self.homenavigationController.view.frame = frame;
        }];
        
        self.sideMenu.isOpened = YES;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:CLOSED_MENU_NOTIFICATION object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            CGRect frame = self.homenavigationController.view.frame;
            frame.origin.x = 0.0f;
            self.homenavigationController.view.frame = frame;
        }];
        
        self.sideMenu.isOpened = NO;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:OPEN_VIEW_CONTROLLER_NOTIFICATION object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if (self.sideMenu.isOpened) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CLOSED_MENU_NOTIFICATION object:nil];
        }
        
        BaseViewController *toViewController = (BaseViewController *)note.object;
        [self.homenavigationController pushViewController:toViewController animated:YES];
    }];
}

#pragma mark - Private API

- (void)configureSideMenuVC {
    self.sideMenu = (SideMenuViewController *)[Helpers getViewControllerFromClass:[SideMenuViewController class]];
    
    //View Controller Containment
    [self addChildViewController:self.sideMenu];
    [self.view addSubview:self.sideMenu.view];
    [self.sideMenu didMoveToParentViewController:self];
}

- (void)configureHomeNavigationController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.homenavigationController = [storyboard instantiateViewControllerWithIdentifier:@"HomeNavigationController"];
    
    //View Controller Containment
    [self addChildViewController:self.homenavigationController];
    [self.view addSubview:self.homenavigationController.view];
    [self.homenavigationController didMoveToParentViewController:self];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForNotifications];
    [self configureSideMenuVC];
    [self configureHomeNavigationController];
}

@end