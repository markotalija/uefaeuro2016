//
//  PlayersViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface PlayersViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *itemsArray;
@property (strong, nonatomic) DataManager *dataManager;
@property (strong, nonatomic) NSMutableDictionary *dictionary;
@end