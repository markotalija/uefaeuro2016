//
//  SideMenuViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "SideMenuItem.h"
#import "DataManager.h"

@interface SideMenuViewController() <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *turkishImageView;
@property (strong, nonatomic) DataManager *dataManager;
@end

@implementation SideMenuViewController

#pragma mark - Properties

- (DataManager *)dataManager {
    return [DataManager sharedInstance];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)configureTurkishImage {
    self.turkishImageView.clipsToBounds = YES;
    self.turkishImageView.layer.cornerRadius = self.turkishImageView.frame.size.width / 8;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTurkishImage];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataManager.sideMenuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.sideMenuItem = [self.dataManager.sideMenuArray objectAtIndex:indexPath.row];
    
    return cell;
}

@end