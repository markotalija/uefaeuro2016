//
//  DataManager.m
//  UEFAEuro2016
//
//  Created by Apple on 7/18/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "DataManager.h"
#import "Player.h"
#import "SideMenuItem.h"

@interface DataManager()
@property (strong, nonatomic) NSMutableArray *playersArray;
@end

@implementation DataManager

#pragma mark - Properties

- (NSMutableArray *)playersArray {
    if (!_playersArray) {
        _playersArray = [[NSMutableArray alloc] init];
    }
    
    return _playersArray;
}

- (NSArray *)sideMenuArray {
    return @[
          [[SideMenuItem alloc] initWithText:@"Matches" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Latest" andImage:[UIImage imageNamed:@"Latest"]],
          [[SideMenuItem alloc] initWithText:@"Groups" andImage:[UIImage imageNamed:@"Groups"]],
          [[SideMenuItem alloc] initWithText:@"Teams" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Players" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Stats" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Ticketing" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Fanzone" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Host cities" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Profile" andImage:[UIImage imageNamed:@"Matches"]],
          [[SideMenuItem alloc] initWithText:@"Settings" andImage:[UIImage imageNamed:@"Matches"]]
          ];
}

#pragma mark - Designated Initializers

+ (instancetype)sharedInstance {
    static DataManager *sharedManager;
    
    @synchronized (self) {
        if (sharedManager == nil) {
            sharedManager = [[DataManager alloc] init];
        }
    }
    
    return sharedManager;
}

#pragma mark - Public API

- (void)fillData {
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Amir" andLastName:@"Abrashi"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Adrien" andLastName:@"Silva"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Ahmet" andLastName:@"Çalık"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Birkir" andLastName:@"Bjarnason"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Bekim" andLastName:@"Balaj"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Barnabás" andLastName:@"Bese"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Caner" andLastName:@"Erkin"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Craig" andLastName:@"Cathcart"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Cédric" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Danilo" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"David" andLastName:@"de Gea"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Daniele" andLastName:@"De Rossi"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Eduardo" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Éder" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Eliseu" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Fraser" andLastName:@"Forster"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Fabian" andLastName:@"Frei"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Fôn" andLastName:@"Williams"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"György" andLastName:@"Garics"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Gökhan" andLastName:@"Gönül"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Guilherme" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Hakan" andLastName:@"Balta"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Hannes" andLastName:@"Halldórsson"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Harun" andLastName:@"Tekin"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Ismail" andLastName:@"Köybaşı"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Ivanov" andLastName:@"Oleg"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Insigne" andLastName:@"Lorenzo"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Jakob" andLastName:@"Jantscher"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"João" andLastName:@"Moutinho"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Jordi" andLastName:@"Alba"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Kim" andLastName:@"Källström"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Koke" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Kukeli" andLastName:@"Burim"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Lucas" andLastName:@"Vázquez"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Lustig" andLastName:@"Mikael"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Long" andLastName:@"Shane"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Mario" andLastName:@"Mandžukić"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Mërgim" andLastName:@"Mavraj"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Michael" andLastName:@"McGovern"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Nani" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Nemanja" andLastName:@"Nikolić"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Nolito" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Olcay" andLastName:@"Şahan"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Onur" andLastName:@"Kıvrak"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Ozan" andLastName:@"Tufan"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Pedro" andLastName:@"Rodriguez"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Peter" andLastName:@"Pekarik"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Pepe" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Quinn" andLastName:@"Stephen"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Quaresma" andLastName:@"Ricardo"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Rafa" andLastName:@"Silva"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Raphael" andLastName:@"Guerreiro"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Renato" andLastName:@"Sanches"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Semih" andLastName:@"Kaya"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Stanislav" andLastName:@"Šestak"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Stefano" andLastName:@"Sturaro"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Thiago" andLastName:@"Alcantara"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Torje" andLastName:@"Gabriel"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Torbinski" andLastName:@"Dmitri"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Umtiti" andLastName:@"Samuel"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Vieirinha" andLastName:@""]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Volkan" andLastName:@"Babacan"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Vokes" andLastName:@"Sam"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Walker" andLastName:@"Kyle"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Williams" andLastName:@"Ashley"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Ward" andLastName:@"Stephen"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Xherdan" andLastName:@"Shaqiri"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Xhaka" andLastName:@"Granit"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Xhaka" andLastName:@"Taulant"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Yarmolenko" andLastName:@"Andriy"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Yunus" andLastName:@"Malli"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Yusupov" andLastName:@"Artur"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Zaza" andLastName:@"Simone"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Zakaria" andLastName:@"Denis"]];
    [self.playersArray addObject:[[Player alloc] initWithFirstName:@"Zinchenko" andLastName:@"Olexandr"]];
    
}

- (NSMutableDictionary *)prepareDictionary {
    NSMutableDictionary *resultsDictionary = [NSMutableDictionary dictionary];
    
    NSArray *lettersArray = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
    for (NSString *letter in lettersArray) {
        NSArray *playersArray = [self getPlayersWhoseNameBeginsWith:letter];
        if (playersArray.count > 0) {
            [resultsDictionary setValue:playersArray forKey:letter];
        }
    }
    
    return resultsDictionary;
}

- (NSArray *)getPlayersWhoseNameBeginsWith:(NSString *)letter {
    NSMutableArray *playersArray = [NSMutableArray array];
    
    for (Player *player in self.playersArray) {
        if ([player.fName hasPrefix:letter]) {
            [playersArray addObject:player];
        }
    }
    
    return playersArray;
}

#pragma mark - NSUserDefaults

+ (void)saveCustomObjectToUserDefaults:(id)object forKey:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)loadCustomObjectFromUserDefaultsForKey:(NSString *)key {
    NSData *encodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    id object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    return object;
}

@end