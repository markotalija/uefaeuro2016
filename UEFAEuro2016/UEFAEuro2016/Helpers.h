//
//  Helpers.h
//  UEFAEuro2016
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

@interface Helpers : NSObject
+ (BaseViewController *)getViewControllerFromClass:(Class)viewControllerClass;
+ (void)openViewControllerWithClass:(Class)viewControllerClass;
@end