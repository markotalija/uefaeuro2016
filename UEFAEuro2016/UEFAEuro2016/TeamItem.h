//
//  TeamItem.h
//  UEFAEuro2016
//
//  Created by Apple on 7/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamItem : NSObject <NSCoding>
@property (strong, nonatomic) UIImage *teamFlag;
@property (strong, nonatomic) NSString *countryString;

- (instancetype)initWithFlag:(UIImage *)teamFlag andCountry:(NSString *)countryString;

@end