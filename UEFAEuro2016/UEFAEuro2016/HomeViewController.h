//
//  HomeViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 7/20/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *sideMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (strong, nonatomic) NSArray *itemsArray;
@end