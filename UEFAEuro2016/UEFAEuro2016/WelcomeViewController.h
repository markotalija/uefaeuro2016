//
//  WelcomeViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 7/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *teamButton;
@property (weak, nonatomic) IBOutlet UIButton *playersButton;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@end