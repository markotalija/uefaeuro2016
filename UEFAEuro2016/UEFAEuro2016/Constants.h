//
//  Constants.h
//  UEFAEuro2016
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//Notifications
static NSString *const OPEN_MENU_NOTIFICATION               = @"OPEN_MENU_NOTIFICATION";
static NSString *const CLOSED_MENU_NOTIFICATION             = @"CLOSED_MENU_NOTIFICATION";
static NSString *const OPEN_VIEW_CONTROLLER_NOTIFICATION    = @"OPEN_VIEW_CONTROLLER_NOTIFICATION";

//Numbers
#define kAnimationDuration 0.3f
#define kMenuOffset SCREEN_WIDTH-69.0f

//Macros
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

//URLs
static NSString *const ADIDAS_URL       = @"http://www.global.adidas.com";
static NSString *const CARLSBERG_URL    = @"http://www.carlsberg.com/#!overview";
static NSString *const COKE_URL         = @"http://www.coca-cola.com/global/glp.html";
static NSString *const CONTINENTAL_URL  = @"http://www.continentaltire.com";
static NSString *const HISENSE_URL      = @"https://www.hisense-usa.com/electronics/tv";
static NSString *const HYUNDAI_URL      = @"http://worldwide.hyundai.com/WW/Main/index.html";
static NSString *const MCDONALDS_URL    = @"http://mcdonalds.rs";
static NSString *const ORANGE_URL       = @"http://www.orange.com/en/home";
static NSString *const SOCAR_URL        = @"http://www.socar.az/socar/az/home/";
static NSString *const TURKISH_URL      = @"http://www.turkishairlines.com";

//Enums
typedef NS_ENUM(NSInteger, Sponsors) {
    ADIDAS = 1,
    CARLSBERG,
    COKE,
    CONTINENTAL,
    HISENSE,
    HYUNDAI,
    MCDONALDS,
    ORANGE,
    SOCAR,
    TURKISH
};

#endif /* Constants_h */
