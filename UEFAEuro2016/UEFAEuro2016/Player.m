//
//  Player.m
//  UEFAEuro2016
//
//  Created by Apple on 7/18/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "Player.h"

@implementation Player

#pragma mark - Designated Initializer

- (instancetype)initWithFirstName:(NSString *)fName andLastName:(NSString *)lName {
    if (self = [super init]) {
        self.fName = fName;
        self.lName = lName;
    }
    
    return self;
}

@end