//
//  PlayersTableViewCell.h
//  UEFAEuro2016
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *playerLabel;
@end