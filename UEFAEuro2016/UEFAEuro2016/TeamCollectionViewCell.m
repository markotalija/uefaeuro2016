//
//  TeamCollectionViewCell.m
//  UEFAEuro2016
//
//  Created by Apple on 7/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "TeamCollectionViewCell.h"

@implementation TeamCollectionViewCell

- (void)setTeamItem:(TeamItem *)teamItem {
    _teamItem = teamItem;
    
    self.flagImageView.image = teamItem.teamFlag;
    self.countryLabel.text = teamItem.countryString;
    
    self.flagImageView.clipsToBounds = YES;
    self.flagImageView.layer.cornerRadius = self.flagImageView.frame.size.width / 2;
}

@end