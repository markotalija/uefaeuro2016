//
//  SideMenuTableViewCell.m
//  UEFAEuro2016
//
//  Created by Apple on 7/26/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "SideMenuTableViewCell.h"

@implementation SideMenuTableViewCell

- (void)setSideMenuItem:(SideMenuItem *)sideMenuItem {
    _sideMenuItem = sideMenuItem;
    
    self.cellTextLabel.text = sideMenuItem.text;
    self.cellImageView.image = sideMenuItem.image;
    self.cellImageView.clipsToBounds = YES;
    self.cellImageView.layer.cornerRadius = self.cellImageView.frame.size.width / 2;
}

@end