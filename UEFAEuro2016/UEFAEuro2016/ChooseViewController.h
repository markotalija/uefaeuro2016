//
//  ChooseViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 7/22/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *teamButton;
@end