//
//  AccountsViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 7/8/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@class GIDSignInButton;

@interface AccountsViewController : UIViewController <GIDSignInUIDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@end