//
//  ChooseViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/22/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ChooseViewController.h"
#import "TeamItem.h"
#import "DataManager.h"

@interface ChooseViewController()
@end

@implementation ChooseViewController

#pragma mark - Actions

- (IBAction)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"team"]) {
        TeamItem *item = (TeamItem *)[DataManager loadCustomObjectFromUserDefaultsForKey:@"team"];
        
        [self.teamButton setImage:item.teamFlag forState:UIControlStateNormal];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end