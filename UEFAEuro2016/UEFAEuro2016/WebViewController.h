//
//  WebViewController.h
//  UEFAEuro2016
//
//  Created by Apple on 8/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (strong, nonatomic) NSString *urlString;
@end