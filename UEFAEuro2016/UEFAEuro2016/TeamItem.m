//
//  TeamItem.m
//  UEFAEuro2016
//
//  Created by Apple on 7/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "TeamItem.h"

@implementation TeamItem

- (instancetype)initWithFlag:(UIImage *)teamFlag andCountry:(NSString *)countryString {
    if (self = [super init]) {
        self.teamFlag = teamFlag;
        self.countryString = countryString;
    }
    
    return self;
}

#pragma mark - NSCoding

    //Necessary for NSUserDefaults
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.teamFlag = [aDecoder decodeObjectForKey:@"teamFlag"];
        self.countryString = [aDecoder decodeObjectForKey:@"countryString"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.teamFlag forKey:@"teamFlag"];
    [aCoder encodeObject:self.countryString forKey:@"countryString"];
}

@end