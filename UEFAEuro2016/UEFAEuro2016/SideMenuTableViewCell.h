//
//  SideMenuTableViewCell.h
//  UEFAEuro2016
//
//  Created by Apple on 7/26/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuItem.h"

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel;
@property (strong, nonatomic) SideMenuItem *sideMenuItem;
@end