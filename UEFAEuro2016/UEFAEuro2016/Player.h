//
//  Player.h
//  UEFAEuro2016
//
//  Created by Apple on 7/18/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject
@property (strong, nonatomic) NSString *fName;
@property (strong, nonatomic) NSString *lName;
- (instancetype)initWithFirstName:(NSString *)fName andLastName:(NSString *)lName;
@end