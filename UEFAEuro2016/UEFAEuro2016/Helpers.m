//
//  Helpers.m
//  UEFAEuro2016
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "Helpers.h"
#import "Constants.h"

@implementation Helpers

#pragma mark - Public API

+ (BaseViewController *)getViewControllerFromClass:(Class)viewControllerClass {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    BaseViewController *toViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(viewControllerClass)];
    
    return toViewController;
}

+ (void)openViewControllerWithClass:(Class)viewControllerClass {
    BaseViewController *toViewController = [Helpers getViewControllerFromClass:viewControllerClass];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_VIEW_CONTROLLER_NOTIFICATION object:toViewController];
}

@end