//
//  TeamCollectionViewCell.h
//  UEFAEuro2016
//
//  Created by Apple on 7/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamItem.h"

@interface TeamCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (strong, nonatomic) TeamItem *teamItem;
@end