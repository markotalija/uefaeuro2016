//
//  HomeViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/20/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "HomeViewController.h"
#import "WebViewController.h"
#import "HomeTableViewCell.h"
#import "Constants.h"

@interface HomeViewController() <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSString *url;
@end

@implementation HomeViewController

#pragma mark - Actions

- (IBAction)plusButtonTapped {
    [self performSegueWithIdentifier:@"ChooseSegue" sender:self];
}

- (IBAction)sideMenuButtonTapped {
    [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_MENU_NOTIFICATION object:nil];
}

- (IBAction)sponsorButtonTapped:(UIButton *)sender {
    
    switch (sender.tag) {
        case ADIDAS:
            self.url = ADIDAS_URL;
            break;
            
        case CARLSBERG:
            self.url = CARLSBERG_URL;
            break;
            
        case COKE:
            self.url = COKE_URL;
            break;
            
        case CONTINENTAL:
            self.url = CONTINENTAL_URL;
            break;
            
        case HISENSE:
            self.url = HISENSE_URL;
            break;
            
        case HYUNDAI:
            self.url = HYUNDAI_URL;
            break;
            
        case MCDONALDS:
            self.url = MCDONALDS_URL;
            break;
            
        case ORANGE:
            self.url = ORANGE_URL;
            break;
            
        case SOCAR:
            self.url = SOCAR_URL;
            break;
            
        case TURKISH:
            self.url = TURKISH_URL;
            break;
    }
    
    [self performSegueWithIdentifier:@"Show Sponsor Segue" sender:self];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[WebViewController class]]) {
        WebViewController *toViewController = segue.destinationViewController;
        toViewController.urlString = self.url;
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cell;
}

#pragma mark - UITableViewDelegate

@end