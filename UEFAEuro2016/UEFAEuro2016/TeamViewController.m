//
//  TeamViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "TeamViewController.h"
#import "TeamItem.h"
#import "TeamCollectionViewCell.h"
#import "DataManager.h"

@interface TeamViewController() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) DataManager *dm;
@end

@implementation TeamViewController

#pragma mark - Private API

- (void)fillCollectionView {
    self.itemsArray = @[[[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Albania"] andCountry:@"Albania"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Austria"] andCountry:@"Austria"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Belgium"] andCountry:@"Belgium"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Croatia"] andCountry:@"Croatia"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"CZ"] andCountry:@"Czech Rep."],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"England"] andCountry:@"England"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"France"] andCountry:@"France"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Germany"] andCountry:@"Germany"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Hungary"] andCountry:@"Hungary"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Iceland"] andCountry:@"Iceland"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Ireland"] andCountry:@"Rep. Ireland"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Italy"] andCountry:@"Italy"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"NIreland"] andCountry:@"N. Ireland"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Poland"] andCountry:@"Poland"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Portugal"] andCountry:@"Portugal"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Romania"] andCountry:@"Romania"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Russia"] andCountry:@"Russia"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Slovakia"] andCountry:@"Slovakia"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Spain"] andCountry:@"Spain"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Sweden"] andCountry:@"Sweden"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Switzerland"] andCountry:@"Switzerland"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Turkey"] andCountry:@"Turkey"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Ukraine"] andCountry:@"Ukraine"],
                        [[TeamItem alloc] initWithFlag:[UIImage imageNamed:@"Wales"] andCountry:@"Wales"],];
}

#pragma mark - Actions

- (IBAction)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fillCollectionView];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TeamCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    TeamItem *item = [self.itemsArray objectAtIndex:indexPath.item];
    cell.teamItem = item;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Save object at selected index so that it shows on WelcomeVC
    TeamItem *item = [self.itemsArray objectAtIndex:indexPath.item];
    [DataManager saveCustomObjectToUserDefaults:item forKey:@"team"];
    
    if (item) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 3.0;
    CGSize rect = CGSizeMake(cellWidth, cellWidth);
    
    return rect;
}

@end