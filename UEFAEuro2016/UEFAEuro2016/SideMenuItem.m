//
//  SideMenuItem.m
//  UEFAEuro2016
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "SideMenuItem.h"

@implementation SideMenuItem

- (instancetype)initWithText:(NSString *)text andImage:(UIImage *)image {
    if (self = [super init]) {
        self.text = text;
        self.image = image;
    }
    
    return self;
}

@end