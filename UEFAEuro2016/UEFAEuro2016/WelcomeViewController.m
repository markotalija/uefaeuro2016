//
//  WelcomeViewController.m
//  UEFAEuro2016
//
//  Created by Apple on 7/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "WelcomeViewController.h"
#import "PushViewController.h"
#import "TeamItem.h"
#import "DataManager.h"
#import "Player.h"

@implementation WelcomeViewController

#pragma mark - Actions

- (IBAction)nextButtonTapped {
    [self performSegueWithIdentifier:@"PushVCSegue" sender:self];
}

- (IBAction)teamButtonTapped {
    [self performSegueWithIdentifier:@"TeamSegue" sender:self];
}

- (IBAction)playersButtonTapped {
    [self performSegueWithIdentifier:@"PlayersSegue" sender:self];
}

- (IBAction)signInButtonTapped {
    [self performSegueWithIdentifier:@"AccountsSegue" sender:self];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //If object exists, load it from Data Manager
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"team"]) {
        TeamItem *item = (TeamItem *)[DataManager loadCustomObjectFromUserDefaultsForKey:@"team"];
        
        //Set selected image from UICollectionView
        [self.teamButton setImage:item.teamFlag forState:UIControlStateNormal];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
